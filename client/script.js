// let task = {id: number, name: string, isDone: Boolean}
let to_render = '';

renderLists();


async function renderLists() {
    to_render = "";
    try {
        const res = await axios.get('/tasklist/render')
        const list = res.data;
        for (let i = 0; i < list.length; i++) {
            to_render += `<div class="box"><label>${list[i].name}</label> <div>
    <input type="checkbox" id="${list[i].id}" onchange="updateTask(id)"`;
            to_render += list[i].isDone ? " checked >" : ">";
            to_render += `<button id="${list[i].id}" onclick="deleteTask(id)" >X</button> </div> </div> `
        }

        document.querySelector('#TaskList').innerHTML = to_render;
    } catch (err) {
        console.log(err);
    }
}


async function addNewTask(event) {
    event.preventDefault();
    const values = Object.values(event.target);
    try {
        const res = await axios.post('/tasklist', {
            action: "addtask",
            name: values[0].value
        })
        console.log(res.data);
        document.querySelector('form').reset();
        renderLists();
    } catch (err) {
        console.log(err);
    }
}

async function deleteTask(id) {
    try {
        const res = await axios.delete(`/tasklist/delete?id=${id}`)
        console.log(res.data);
        renderLists();
    } catch (err) {
        console.log(err);
    }
}

async function updateTask(id) {
    try {
        const res = await axios.put('/tasklist', {
            action: "update",
            id: id
        })
        console.log(res.data);
        renderLists();
    } catch (err) {
        console.log(err);
    }
}


document.querySelector('form').onsubmit = addNewTask;


