require('dotenv').config()
const express = require('express');
const app = express();

// This is to parse any json objects to JS objects.
// json objects are passed here from postman.
// JS code that access the server uses JS object rather than JSON
app.use(express.json());

console.log(__filename);
console.log(__dirname);
//do not use username because it is reserved
// console.log(process.env.username1);


//tells the server to open index.html in the following folder
app.use(express.static('client'));

// cors is a layer that enables the server to block access based on different
// parameters. 
// Browsers, in the desire to protect users from accessing areas they should not
// access, uses these capabilities of the server.
// postman never uses these capabilities and therefor never block access
// currently this server does not make use of this layer 
// note: we do not need this if we access from the same domain
const cors = require('cors');
app.use(cors())

const fs = require('fs');

// functions to read the files from the FS every time we need to access the list of tasks
// or the id counter.
// before we had a permanent JS array that we were syncing with the FS each time.
// this was not a good practice.
function readListOftasksFromFS() {
    return (fs.existsSync('listOfTasks.json') ? JSON.parse(fs.readFileSync('listOfTasks.json', 'utf-8')) : []);
}

function readJsonListOfTasksFromFS() {
    return fs.readFileSync('listOfTasks.json', 'utf-8');
}

function readIdCounterFromFS() {
    return Number(fs.existsSync('taskIdCounter.txt') ? fs.readFileSync('taskIdCounter.txt') : 0);
}

// app.get('/', function (req, res) {
//     res.send('Hello World')
// })

//  to use from postman GET
//http://localhost:3000/tasklist/render
app.get('/tasklist/:action', function (req, res) {
    // console.log('params', req.params);
    if (req.params.action == 'render')
        // res.send(JSON.stringify(list));
        res.send(readJsonListOfTasksFromFS());
    else
        res.send(`illegal action`);
})

//  to use from postman DELETE
//http://localhost:3000/tasklist/delete?id=1
app.delete('/tasklist/:action', function (req, res) {
    // console.log('params', req.params);
    // console.log('query', req.query);

    const id = Number(req.query.id);
    if (req.params.action == 'delete') {
        console.log(id);
        const list = readListOftasksFromFS();
        for (let i = 0; i < list.length; i++) {
            if (list[i].id == id) {
                list.splice(i, 1);

                // writing the file to the FS
                fs.writeFileSync('listOfTasks.json', JSON.stringify(list, null, 4));

                break;
            }
        }
        res.send('deleted');
    }
    else
        res.send(`illegal action`);
})


// to use from postman POST
//http://localhost:3000/tasklist
//and in the body put the following json:
//{
//     "action": "addtask",
//     "name": "task2"
// }
app.post('/tasklist', function (req, res) {
    // console.log(req.body);
    // console.log('params', req.body.action);
    // console.log('query', req.body.name);
    const task_name = req.body.name;
    // const task_id = req.body.id;

    if (req.body.action == 'addtask') {
        const list = readListOftasksFromFS();
        let id_counter = readIdCounterFromFS();
        list.push({ id: id_counter++, name: task_name, isDone: false });

        // writing the file to the FS
        fs.writeFileSync('listOfTasks.json', JSON.stringify(list, null, 4));
        fs.writeFileSync('taskIdCounter.txt', String(id_counter));

        res.send('done');

    }
    else
        res.send(`illegal action`);

})


// to use from postman PUT
//http://localhost:3000/tasklist
//body:
//{
//     "action": "update",
//     "id": "2"
// }
app.put('/tasklist', function (req, res) {
    // console.log(req.body);
    // console.log('params', req.body.action);
    // console.log('query', req.body.id);
    const id = req.body.id;

    if (req.body.action == 'update') {

        // console.log(id);
        const list = readListOftasksFromFS();
        for (let i = 0; i < list.length; i++) {
            if (list[i].id == id) {
                list[i].isDone = !list[i].isDone;

                // writing the file to the FS
                fs.writeFileSync('listOfTasks.json', JSON.stringify(list, null, 4));

                break;
            }
        }
        console.log(...list)

        res.send("updated");
    }
    else
        res.send(`illegal action`);

})


app.listen(3000, () => {
    console.log('server is running');
})

