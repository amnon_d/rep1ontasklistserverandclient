this version uses index1.html, script.js and style.css to 
show a list of tasks.
the html has a place holder for a rendered html that will 
come from the script.
the script has methods for showing, adding, updating and deleting.
These methods uses REST apis from the server
At the end of each method it calls the method that renders the list.
the script uses local storage for the counter for the id but not for
the list itself. in fact it does not store the list as it is stored in
the server.
in index.html we put:
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
in the head section.
This method saves us installing the axios package

added: list of tasks and the id counter are kept in the FS

Inherited from the InitialBranch:
const cors = require('cors');
app.use(cors());
cors is a layer that enables the server to block access based on different parameters. 
Browsers, in the desire to protect users from accessing areas they should not
access, uses these capabilities of the server.
postman never uses these capabilities and therefor never block access
currently this server does not make use of this layer 
note: we do not need this if we access from the same domain, that is
when the server serves index.html upon serfing to localhost:3000

Added for branch1:
==================
1. using dotenv
   package dotenv was installed: npm i dotenv
   .env file was added with some dummy entries and the server printss
   those to the console
2. added: app.use(express.static('client')); which tells the server
to serve the client index.html (the default) when we open the browser
on localhost:3000
for this we had to move index.html, style.css and script.js to sub folder 'client'
Also note that starting the client by clicking index.html will not work anymore.
these 3 files were deleted from the main folder.
In addition to that, the client need not specify the full url in the call
to the REST api in the server, so:
axios.get('http://localhost:3000/tasklist/render')
becomes:
    axios.get('/tasklist/render');
3.  script.js uses async-await when calling axios functions. .then blocks
were removed.

Added for branch2:
==================
All calls to REST apis to the server are wrapped with try-catch
note that when we used the non async-await version we needed only to 
append .catch to the .then since promises has try block inside.